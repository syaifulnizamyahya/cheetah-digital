﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cheetah
{
    public class Recipient
    {
        public HashSet<string> tags { set; get; }
        public string name;
        public int id;
    }

    public class RecipientRoot
    {
        public List<Recipient> recipients { set; get; }
    }
}
