﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Cheetah
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = JsonConvert.DeserializeObject<RecipientRoot>(File.ReadAllText(@"data.json"));

            for (int i = 0; i < root.recipients.Count; i++)
            {
                for (int j = i + 1; j < root.recipients.Count; j++)
                {
                    var iTagsCount = root.recipients[i].tags.Count;
                    var jTagsCount = root.recipients[j].tags.Count;
                    if (iTagsCount >= 2 && jTagsCount >= 2)
                    {
                        int sameTag = 0;
                        foreach (var tag in root.recipients[i].tags)
                        {
                            if (root.recipients[j].tags.Contains(tag))
                            {
                                sameTag += 1;
                            }
                        }
                        if (sameTag >= 2)
                        {
                            Console.Write("{0}, {1}|", root.recipients[i].name, root.recipients[j].name);
                        }
                    }
                }
            }
        }
    }
}
